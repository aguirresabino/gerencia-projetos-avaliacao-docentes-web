(function(){
    'use strict';

    angular.module('AlunoController', ['AlunoService'])
        .controller('AlunoController', ['AlunoService', AlunoController]);

    function AlunoController(AlunoService){
        var vm = this;

        vm.cadastrar = function(aluno){
            AlunoService.create(aluno)
                .then(function(success){
                    console.log("Cadastro realizado com sucesso!");
                })
                .catch(function(error){
                    console.log("Cadastro não foi realizado: ", error);
                })
        }

        vm.ler = function(aluno){

        }

        vm.atualizar = function(aluno){

        }

        vm.remover = function(aluno){

        }
    }

})();