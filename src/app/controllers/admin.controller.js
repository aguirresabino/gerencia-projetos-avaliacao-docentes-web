(function(){
    'use strict';

    angular.module('AdminController', ['AdminService'])
        .controller('AdminController', ['AdminService', AdminController]);

    function AdminController(AdminService){
        var vm = this;

        vm.cadastrar = function(admin){
            AdminService.create(admin)
                .then(function(success){
                    console.log("Cadastro realizado com sucesso!");
                })
                .catch(function(error){
                    console.log("Cadastro não foi realizado: ", error);
                })
        }

        vm.ler = function(admin){

        }

        vm.atualizar = function(admin){

        }

        vm.remover = function(admin){

        }
    }

})();