(function(){
	'use strict';

	angular.module('AlunoService', [])
		.service('AlunoService', AlunoService);

	function AlunoService($http){
		const urlBase = 'https://avaliacao-docentes.herokuapp.com/rest';
		const methods = {
			GET : 'GET',
			POST : 'POST',
			PUT : 'PUT',
			DELETE : 'DELETE'
		};

		this.login = function(login){
			let path = `login/loginAluno/${login.matricula}/${login.senha}`;

			let request = {
				url: `${urlBase}/${path}`,
				method: methods.POST
			};

			return $http(request);
		}

		this.create = function(aluno){
			let path = 'aluno/cadastrarAluno';

			let request = {
				url: `${urlBase}/${path}`,
				method: methods.POST,
				data: aluno		
			};

			return $http(request);
		}

		/*this.read = function(aluno){

		}

		this.update = function(aluno){
			
		}

		this.delete = function(id){
		
		}*/
	}
})();