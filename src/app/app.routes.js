(function(){
	'use strict';

	angular.module('GpsSite')
		.config(config)

	function config($routeProvider){
		$routeProvider
		    .when('/login', {
		    	templateUrl: '../views/login.html',
		    	controller: 'LoginController',
		    	controllerAs: 'Login'
			})
			.when('/adm-principal', {
				templateUrl: '../views/adm-principal.html',
				controller: 'AdminController',
				controllerAs: 'Admin'
			})
			.when('/cadastrar-adm', {
				templateUrl: '../views/cadastrar-adm.html',
				controller: 'AdminController',
				controllerAs: 'Admin'
			})
			.when('/cadastrar-aluno', {
				templateUrl: '../views/cadastrar-aluno.html',
				controller: 'AlunoController',
				controllerAs: 'Aluno'
			})
		    .otherwise({
		        redirectTo: '/login'
		    })
	}
	config.$inject = ['$routeProvider']
})();