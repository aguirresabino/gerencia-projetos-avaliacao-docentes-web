(function(){
	'use strict';

	angular.module('AdminService', [])
		.service('AdminService', AdminService);

	function AdminService($http){
		const urlBase = 'https://avaliacao-docentes.herokuapp.com/rest';
		const methods = {
			GET : 'GET',
			POST : 'POST',
			PUT : 'PUT',
			DELETE : 'DELETE'
		};

		this.login = function(login){
			let path = `login/loginAdmin/${login.email}/${login.senha}`;

			let request = {
				url: `${urlBase}/${path}`,
				method: methods.POST
			};

			return $http(request);
		}

		this.create = function(admin){
			let path = 'admin/cadastrarAdmin';

			let request = {
				url: `${urlBase}/${path}`,
				method: methods.POST,
				data: admin		
			};

			return $http(request);
		}

		/*this.read = function(admin){

		}

		this.update = function(admin){
			
		}

		this.delete = function(id){
		
		}*/
	}
})();